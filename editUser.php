<?php
require_once ("class/DataBase.php");

$base = DataBase::getDB(); //подключение

$name = htmlentities(mysqli_real_escape_string($base->getMysqli(), $_POST['name']));
$date = htmlentities(mysqli_real_escape_string($base->getMysqli(), $_POST['date']));
$phone = htmlentities(mysqli_real_escape_string($base->getMysqli(), $_POST['phone']));
$mail = htmlentities(mysqli_real_escape_string($base->getMysqli(), $_POST['mail']));
$car = htmlentities(mysqli_real_escape_string($base->getMysqli(), $_POST['car']));



if($_POST['name'] && $_POST['date'] && $_POST['phone'] && ($_POST['car'] !== "Выберите вариант")) {//Проверка заполнености полей
    //Проверка фамилии
    if (preg_match('/^[А-Я]{1}[А-Я а-я -]*$/u', $_POST['name']) == false && preg_match('/^[А-Я]{1}[-]{1}[А-Я а-я -]*$/u', $_POST['name']) == false) {
        echo "Поле фамилия заполняется только буквы и символ «-», первая буква большая.";
        die;
    } else if ($_POST['mail'] !== "") { //Проверка почты
        if (filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL) == false) {
            echo "Адрес электронной почты – шаблон « ___ @. ___ ».";
            die;
        }
    }
    $result = $base->editUser($name,$date,$phone,$mail,$car);
    if ($result) {
        echo "Данные изменены";
    }
}
else echo "Данные не введены";