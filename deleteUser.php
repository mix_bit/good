<?php
require_once ("class/DataBase.php");

$base = DataBase::getDB(); //подключение

$phone = htmlentities(mysqli_real_escape_string($base->getMysqli(), $_POST['phone']));

if($_POST['phone']) {
    $result = $base->deleteUser($phone);
    if ($result) {
        echo "Данные удалены";
    }
}
else echo "Номер телефона не введен";