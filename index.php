<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/form.js"></script>
</head>
<body>
<div>
    <div>
        <h1>Простейшая форма</h1>
        <form method="post">
            <p><label>Фамилия:<br>
                    <input id="name" type="text" value="" "></label></p>
            <p><label>Дата рождения:<br>
                    <input id="date"  type="date" required></label></p>
            <p><label>Номер телефона:<br>
                    <input id="phone" maxlength="11" type="tel" required ></label></p>
            <p><label>Адрес электронной почты:<br>
                    <input id="mail" size="30" type="mail"></label></p>
            <p><label>Марка авто:<br>
                    <select id="car" required>
                        <option selected="selected">Выберите вариант</option>
                        <option>Volkswagen</option>
                        <option>Audi</option>
                        <option>BMW</option>
                        <option>Mercedes</option>
                        <option>Skoda</option>
                    </select>
            <p><input  class="button" id="send" name="register" type="submit"  value="Добавить">
            <input  class="button" id="edit" name="register" type="submit"  value="Обновить">
            <input  class="button" id="delete" name="register" type="submit"  value="Удалить"></p>
        </form>
        <div id="result_form"></div>
        <p>Кнопки для работы с таблицей</p>
        <p ><input   class="button" id="sortPhone" name="register" type="submit"  value="Сортировка по телефону">
            <input  class="button" id="sortName" name="register" type="submit"  value="Сортировка по Фамилии">
            <input  class="button" id="sortDate" name="register" type="submit"  value="Сортировка по Дате Рождения"></p>
        <p><input  class="button" id="backTable" name="register" type="submit"  value="Предыдушая таблица">
            <input  class="button" id="nextTable" name="register" type="submit"  value="Следующиая таблица"></p>
        <div></div>
    </div>
</div>
</body>
</html>

<?php
require_once ("class/DataBase.php");

$base = DataBase::getDB();
$query ="SELECT * FROM user limit 5";

$result = mysqli_query($base->getMysqli(), $query) or die("Ошибка " . mysqli_error($base->getMysqli()));

echo "<table border='1' id='table' >";
echo "<tr><td>Фамилия</td><td>Дата Рождения</td><td>Номер Телефона</td><td>Email</td><td>Марка Авто</td></tr>";
while ($row=mysqli_fetch_array($result)){

    $pole1=$row[0];
    $pole2=$row[1];
    $pole3=$row[2];
    $pole4=$row[3];
    $pole5=$row[4];


    echo "<tr><td align='center'>$pole1</td><td align='center'>$pole2</td><td align='center'>$pole3</td><td align='center'>$pole4</td><td align='center'>$pole5</td></tr>";
}
echo "</table>";
?>
