$(document).ready(function(){
    var currentPage = 0;
    var currentType = 'mail';

    $("#send").click(function() {
        console.log("sendRun");
        var name = $("#name").val();
        var date = $("#date").val();
        var phone = $("#phone").val();
        var mail = $("#mail").val();
        var car = $("#car").val();

        var dataString = 'name=' + name + '&date=' + date + '&phone=' + phone + '&mail=' + mail + '&car=' + car;
        //alert (dataString);return false;
        $.ajax({
            url: "send.php",
            type: "POST",
            data: dataString,
            success: function (result) {
                $("#result_form").html(result);
            },
            error: function(response) { // Данные не отправлены
                $('#result_form').html('Ошибка. Данные не отправлены.');
            }
        });
        return false;
    });

    $("#edit").click(function() {
        console.log("editRun");
        var name = $("#name").val();
        var date = $("#date").val();
        var phone = $("#phone").val();
        var mail = $("#mail").val();
        var car = $("#car").val();
        var dataString = 'name=' + name + '&date=' + date + '&phone=' + phone + '&mail=' + mail + '&car=' + car;
        $.ajax({
            url: "editUser.php",
            type: "POST",
            data: dataString,
            success: function (result) {
                $("#result_form").html(result);
            },
            error: function(response) { // Данные не отправлены
                $('#result_form').html('Ошибка. Данные не отправлены.');
            }
        });
        return false;
    });

    $("#delete").click(function() {
        console.log("deleteRun");
        var phone = $("#phone").val();
        var dataString = 'phone=' + phone;
        $.ajax({
            url: "deleteUser.php",
            type: "POST",
            data: dataString,
            success: function (result) {
                $("#result_form").html(result);
            },
            error: function(response) { // Данные не отправлены
                $('#result_form').html('Ошибка. Данные не отправлены.');
            }
        });
        return false;
    });

    $("#sortDate").click(function() {
        console.log("sortDateRun");
        currentType = 'date';
        $.ajax({
            url: "sort.php",
            type: "GET",
            data: 'type=date&page=0',
            success: function (result) {
                $("#table").html(result);
            },
            error: function(response) { // Данные не отправлены
                $('#table').html('Ошибка. Данные не отправлены.');
            }
        });
        return false;
    });
    $("#sortName").click(function() {
        console.log("sortNameRun");
        currentType = 'name';
        $.ajax({
            url: "sort.php",
            type: "GET",
            data: 'type=name&page=0',
            success: function (result) {
                $("#table").html(result);
            },
            error: function(response) { // Данные не отправлены
                $('#table').html('Ошибка. Данные не отправлены.');
            }
        });
        return false;
    });

    $("#sortPhone").click(function() {
        console.log("sortPhoneRun");
        currentType = 'phone';
        $.ajax({
            url: "sort.php",
            type: "GET",
            data: 'type=phone&page=0',
            success: function (result) {
                $("#table").html(result);
            },
            error: function(response) { // Данные не отправлены
                $('#table').html('Ошибка. Данные не отправлены.');
            }
        });
        return false;
    });

    $("#nextTable").click(function() {
        console.log("nextTable");
        currentPage++;
        $.ajax({
            url: "sort.php",
            type: "GET",
            data: 'type='+currentType+'&page='+currentPage,
            success: function (result) {
                $("#table").html(result);
            },
            error: function(response) { // Данные не отправлены
                $('#table').html('Ошибка. Данные не отправлены.');
            }
        });
        return false;
    });
    $("#backTable").click(function() {
        console.log("nextTable");
        currentPage--;
        $.ajax({
            url: "sort.php",
            type: "GET",
            data: 'type='+currentType+'&page='+currentPage,
            success: function (result) {
                $("#table").html(result);
            },
            error: function(response) { // Данные не отправлены
                $('#table').html('Ошибка. Данные не отправлены.');
            }
        });
        return false;
    });
});

